<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
<?php
  // If the user has the permissions, print an extra row for adding product groups
  if (user_access('create product_group content')) {
    $extra_row = '<div class="product-group-sidebar-item">
      <div class="views-field views-field-title">
        <span class="field-content"><span class="glyphicon glyphicon-plus text-white"></span><a href="/node/add/product-group"> Add Product Group</a></span>
      </div>
    </div>';
    print($extra_row);  
  }
?>
