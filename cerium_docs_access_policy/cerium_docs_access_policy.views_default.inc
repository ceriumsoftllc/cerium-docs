<?php
/**
 * @file
 * cerium_docs_access_policy.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cerium_docs_access_policy_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'access_policies';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Access Policies';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Access Policies';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'og_context';
  $handler->display->display_options['access']['perm'] = 'administer_access_policies';
  $handler->display->display_options['access']['group_id_arg'] = '1';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<p><em>By default, users must be added manually in order to access documentation. However, you may define special access policies that are checked on user login, or by manual activation.</em></p>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Access Check */
  $handler->display->display_options['fields']['field_access_check']['id'] = 'field_access_check';
  $handler->display->display_options['fields']['field_access_check']['table'] = 'field_data_field_access_check';
  $handler->display->display_options['fields']['field_access_check']['field'] = 'field_access_check';
  /* Field: Field: Access Tags */
  $handler->display->display_options['fields']['field_access_tags']['id'] = 'field_access_tags';
  $handler->display->display_options['fields']['field_access_tags']['table'] = 'field_data_field_access_tags';
  $handler->display->display_options['fields']['field_access_tags']['field'] = 'field_access_tags';
  $handler->display->display_options['fields']['field_access_tags']['empty'] = 'n/a';
  $handler->display->display_options['fields']['field_access_tags']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['fields']['field_access_tags']['delta_offset'] = '0';
  /* Field: Content: API Endpoint */
  $handler->display->display_options['fields']['field_access_api_endpoint']['id'] = 'field_access_api_endpoint';
  $handler->display->display_options['fields']['field_access_api_endpoint']['table'] = 'field_data_field_access_api_endpoint';
  $handler->display->display_options['fields']['field_access_api_endpoint']['field'] = 'field_access_api_endpoint';
  $handler->display->display_options['fields']['field_access_api_endpoint']['empty'] = 'n/a';
  /* Field: Content: API User */
  $handler->display->display_options['fields']['field_access_api_user']['id'] = 'field_access_api_user';
  $handler->display->display_options['fields']['field_access_api_user']['table'] = 'field_data_field_access_api_user';
  $handler->display->display_options['fields']['field_access_api_user']['field'] = 'field_access_api_user';
  $handler->display->display_options['fields']['field_access_api_user']['empty'] = 'n/a';
  /* Field: Content: Validate users with these roles */
  $handler->display->display_options['fields']['field_access_api_valid_roles']['id'] = 'field_access_api_valid_roles';
  $handler->display->display_options['fields']['field_access_api_valid_roles']['table'] = 'field_data_field_access_api_valid_roles';
  $handler->display->display_options['fields']['field_access_api_valid_roles']['field'] = 'field_access_api_valid_roles';
  $handler->display->display_options['fields']['field_access_api_valid_roles']['label'] = 'API roles to check for';
  $handler->display->display_options['fields']['field_access_api_valid_roles']['empty'] = 'n/a';
  $handler->display->display_options['fields']['field_access_api_valid_roles']['delta_offset'] = '0';
  /* Field: Content: Grant this role on validation */
  $handler->display->display_options['fields']['field_access_role_to_grant']['id'] = 'field_access_role_to_grant';
  $handler->display->display_options['fields']['field_access_role_to_grant']['table'] = 'field_data_field_access_role_to_grant';
  $handler->display->display_options['fields']['field_access_role_to_grant']['field'] = 'field_access_role_to_grant';
  $handler->display->display_options['fields']['field_access_role_to_grant']['label'] = 'Granted Roles';
  $handler->display->display_options['fields']['field_access_role_to_grant']['delta_offset'] = '0';
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = 'Edit';
  /* Field: Content: Delete link */
  $handler->display->display_options['fields']['delete_node']['id'] = 'delete_node';
  $handler->display->display_options['fields']['delete_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['delete_node']['field'] = 'delete_node';
  $handler->display->display_options['fields']['delete_node']['label'] = 'Delete';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Groups audience (og_group_ref) */
  $handler->display->display_options['arguments']['og_group_ref_target_id']['id'] = 'og_group_ref_target_id';
  $handler->display->display_options['arguments']['og_group_ref_target_id']['table'] = 'og_membership';
  $handler->display->display_options['arguments']['og_group_ref_target_id']['field'] = 'og_group_ref_target_id';
  $handler->display->display_options['arguments']['og_group_ref_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['og_group_ref_target_id']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['og_group_ref_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['og_group_ref_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['og_group_ref_target_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['og_group_ref_target_id']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['og_group_ref_target_id']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['og_group_ref_target_id']['validate_options']['types'] = array(
    'product_group' => 'product_group',
  );
  $handler->display->display_options['arguments']['og_group_ref_target_id']['validate_options']['access'] = TRUE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'group_access_policy' => 'group_access_policy',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'node/%/access-policies';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Access Policies';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['access_policies'] = $view;

  return $export;
}
