<?php
/**
 * @file
 * cerium_docs_access_policy.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cerium_docs_access_policy_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cerium_docs_access_policy_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function cerium_docs_access_policy_node_info() {
  $items = array(
    'group_access_policy' => array(
      'name' => t('Group Access Policy'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
