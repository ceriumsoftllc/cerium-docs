<?php
/**
 * @file
 * cerium_docs_access_policy.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function cerium_docs_access_policy_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|group_access_policy|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'group_access_policy';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title',
        1 => 'body',
      ),
      'left' => array(
        2 => 'field_access_check',
      ),
      'right' => array(
        3 => 'field_access_tags',
        4 => 'field_access_api_endpoint',
        5 => 'field_access_api_user',
        6 => 'field_access_api_password',
        7 => 'field_access_api_valid_roles',
      ),
      'footer' => array(
        8 => 'field_access_role_to_grant',
        9 => 'og_group_ref',
        10 => 'path',
      ),
      'hidden' => array(
        11 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'body' => 'header',
      'field_access_check' => 'left',
      'field_access_tags' => 'right',
      'field_access_api_endpoint' => 'right',
      'field_access_api_user' => 'right',
      'field_access_api_password' => 'right',
      'field_access_api_valid_roles' => 'right',
      'field_access_role_to_grant' => 'footer',
      'og_group_ref' => 'footer',
      'path' => 'footer',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|group_access_policy|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'user|user|form';
  $ds_layout->entity_type = 'user';
  $ds_layout->bundle = 'user';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'account',
        1 => 'field_access_tags',
        2 => 'og_user_node',
        3 => 'timezone',
        4 => 'masquerade',
      ),
      'hidden' => array(
        5 => 'field_access_previous_login_date',
        6 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'account' => 'ds_content',
      'field_access_tags' => 'ds_content',
      'og_user_node' => 'ds_content',
      'timezone' => 'ds_content',
      'masquerade' => 'ds_content',
      'field_access_previous_login_date' => 'hidden',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['user|user|form'] = $ds_layout;

  return $export;
}
