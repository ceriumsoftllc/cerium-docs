<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php
  $output .= '&nbsp;&nbsp;';
  static $topic_access_data;
  if (!isset($topic_access_data)) {
    $topic_access_data[$row->nid] = &drupal_static(__FUNCTION__);
  }
  // Create the add buttons for content types
  $types = function_exists('cerium_docs_core_content_types_ct_list') ? cerium_docs_core_content_types_ct_list() : array();
  foreach ($types as $type) {
    if (!isset($topic_access_data['create ' . $type . ' content'])) {
      $topic_access_data['create ' . $type . ' content'] = user_access('create ' . $type . ' content') ? TRUE : FALSE;
    }
    $output .= $topic_access_data['create ' . $type . ' content'] ? 
    '<a href="/node/add/' . str_replace('_', '-', $type) . '?field_topic=' . $row->nid . '&og_group_ref=' . $view->args[1] . '&destination=' . current_path()
    . '" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-plus"></span> <span class="glyphicon ' . cerium_docs_core_content_types_icon_list($type) . '"></span></a> '
    : '';
  }
  
  // Create the edit/delete buttons
  global $user;
  if (!isset($topic_access_data[$row->nid]['edit_node'])) {
    $topic_access_data[$row->nid]['edit_node'] = node_access('update', $row->nid, $user) ? TRUE : FALSE;
  }
  if (!isset($topic_access_data[$row->nid]['delete_node'])) {
    $topic_access_data[$row->nid]['delete_node'] = node_access('delete', $row->nid, $user) ? TRUE : FALSE;
  }
  
  $output .= $topic_access_data[$row->nid]['edit_node'] ? ' <a href="/node/' . $row->nid . '/edit" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-pencil"></span></a>' : NULL;
  $output .= $topic_access_data[$row->nid]['delete_node'] ? ' <a href="/node/' . $row->nid . '/delete" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a>' : NULL;
  print $output; 
?>

<?php
/*
 * [title] <a class="btn btn-xs btn-success" target="_blank" href="/node/add/text-documentation?field_topic=!1&og_group_ref=!2" title="Add Text/HTML Documentation"><span class="glyphicon glyphicon-plus"></span><span class="glyphicon glyphicon-list-alt"></span></a>  <a class="btn btn-xs btn-success" target="_blank" href="/node/add/video-documentation?field_topic=!1&og_group_ref=!2" title="Add Video Documentation"><span class="glyphicon glyphicon-plus"></span><span class="glyphicon glyphicon-film"></span></a>  <a class="btn btn-xs btn-success" target="_blank" href="/node/add/file-documentation?field_topic=!1&og_group_ref=!2" title="Add File Documentation"><span class="glyphicon glyphicon-plus"><span class="glyphicon glyphicon-file"></span></span></a> &nbsp;&nbsp;&nbsp;
 */
?>
