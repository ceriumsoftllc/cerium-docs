<?php
/**
 * @file
 * cerium_docs_core_content_types.features.inc
 */

/**
 * Implements hook_views_api().
 */
function cerium_docs_core_content_types_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function cerium_docs_core_content_types_node_info() {
  $items = array(
    'documentation_topic' => array(
      'name' => t('Documentation Topic'),
      'base' => 'node_content',
      'description' => t('This is a container for the actual documentation'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'file_documentation' => array(
      'name' => t('File Documentation'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'link_documentation' => array(
      'name' => t('Link Documentation'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product' => array(
      'name' => t('Product'),
      'base' => 'node_content',
      'description' => t('Products can either be basic products, or subsections of a large product (in which case the product group is the product).'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product_group' => array(
      'name' => t('Product Group'),
      'base' => 'node_content',
      'description' => t('Product groups contain products, and are generally the top-level content for the site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'text_documentation' => array(
      'name' => t('Text/HTML Documentation'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'video_documentation' => array(
      'name' => t('Video Documentation'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
