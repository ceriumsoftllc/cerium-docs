<?php
/**
 * @file
 * Definition of cerium_docs_core_content_types_handler_ct_icon_field
 */
  
/**
 * Provides a custom views field.
 */
class cerium_docs_core_content_types_handler_ct_icon_field extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['cerium_docs_core_content_types_nid_source'] = 'nid';
    return $options;
  }
  
  function cerium_docs_core_content_types_create_option_list() {
    $this->view->execute();
    $ret = array();
    foreach($this->view->field as $key => $f) {
      $ret[$key] = $key . ' - ' . $f->definition['title'];
    }
    return $ret;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['cerium_docs_core_content_types_nid_source'] = array(
      '#type' => 'select',
      '#title' => t('Node ID data source'),
      '#description' => t('Select the field key that points to an NID 
      with the content you want to generate an icon for. Default is 
      base content (nid), other fields are based on your tokens.'),
      '#options' => $this->cerium_docs_core_content_types_create_option_list(),
      '#default_value' => $this->options['cerium_docs_core_content_types_nid_source'],
    );
    $form['cerium_docs_core_content_types_nid_source']['#options']['base_nid'] = 'base_nid';
  }
  
  function query() {
    // do nothing -- to override the parent query.
  }
  
  function render($data) {
    $fields = array('base_nid' => 'nid');
    foreach($this->view->field as $key => $f) {
      if ($key !== 'nid') {
        $fields[$key] = $f->field_alias;
        if (isset($f->aliases) && !empty($f->aliases)) {
          foreach($f->aliases as $k => $alias) {
            if (!($k === 'nid' && $alias === 'nid')) {
              $fields[$k] = $alias;
            }
          }
        }
      }
    }
    $opt = $this->options['cerium_docs_core_content_types_nid_source'];
    if (isset($fields[$opt]) && isset($data->{$fields[$opt]}) && !empty($data->{$fields[$opt]})) {
      $n = node_load($data->{$fields[$opt]});
      $output = sprintf('<span class="glyphicon %s"></span>', cerium_docs_core_content_types_icon_list($n->type));
    }
    else {
      $output = '';
    }
    return $output; 
  }
}
