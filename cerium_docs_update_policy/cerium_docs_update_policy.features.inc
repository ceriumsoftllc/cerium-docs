<?php
/**
 * @file
 * cerium_docs_update_policy.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cerium_docs_update_policy_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "elysia_cron" && $api == "default_elysia_cron_rules") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cerium_docs_update_policy_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function cerium_docs_update_policy_node_info() {
  $items = array(
    'update_policy' => array(
      'name' => t('Update Policy'),
      'base' => 'node_content',
      'description' => t('Contains information to schedule update requests, or to mark items as potentially obsolete. These may be attached to products, product topics, or individual pieces of documentation, and policies work on the basis of general -> specific.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
