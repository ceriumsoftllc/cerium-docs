<?php
/**
 * @file
 * cerium_docs_update_policy.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function cerium_docs_update_policy_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|update_policy|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'update_policy';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title',
        1 => 'field_policy_item',
      ),
      'left' => array(
        2 => 'field_policy_type',
        3 => 'field_relationship_to_parents',
      ),
      'right' => array(
        4 => 'field_duration',
      ),
      'footer' => array(
        5 => 'og_group_ref',
        6 => 'path',
        7 => 'field_trigger_date',
      ),
      'hidden' => array(
        8 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'field_policy_item' => 'header',
      'field_policy_type' => 'left',
      'field_relationship_to_parents' => 'left',
      'field_duration' => 'right',
      'og_group_ref' => 'footer',
      'path' => 'footer',
      'field_trigger_date' => 'footer',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|update_policy|form'] = $ds_layout;

  return $export;
}
