<?php
/**
 * @file
 * Definition of cerium_docs_update_policy_handler_update_status_field
 */
  
/**
 * Provides a custom views field.
 */
class cerium_docs_update_policy_handler_update_status_field extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['cerium_docs_update_policy_update_status_text_true'] = 'Update Required';
    $options['cerium_docs_update_policy_update_status_text_false'] = 'No Update Required';
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['cerium_docs_update_policy_update_status_text_true'] = array(
      '#type' => 'textfield',
      '#title' => t('Text for when update is needed'),
      '#description' => t('Enter the text/HTML you want to have appear 
      when an update is needed for the entity. Allowed HTML tags: 
      div p span a em strong cite blockquote code ul ol li'),
      '#default_value' => $this->options['cerium_docs_update_policy_update_status_text_true'],
    );
    $form['cerium_docs_update_policy_update_status_text_false'] = array(
      '#type' => 'textfield',
      '#title' => t('Text for when update is not needed'),
      '#description' => t('Enter the text/HTML you want to have appear 
      when no update is needed for the entity. Allowed HTML tags: 
      div p span a em strong cite blockquote code ul ol li'),
      '#default_value' => $this->options['cerium_docs_update_policy_update_status_text_false'],
    );
  }
  
  function query() {
    // do nothing -- to override the parent query.
  }
  
  function render($data) {
    // Get the relevant nid
    $nid = $this->relationship ? $data->{$this->relationship . '_nid'} : $data->nid;
    if (!$nid) {
      return '';
    }
    $node = node_load($nid);
    $update_needed = cerium_docs_update_policy_check_policies($node);
    $allowed_tags = explode(' ', 'div p span a em strong cite blockquote code ul ol li');
    if ($update_needed) {
      $output = filter_xss($this->options['cerium_docs_update_policy_update_status_text_true'], $allowed_tags);
    }
    else {
      $output = filter_xss($this->options['cerium_docs_update_policy_update_status_text_false'], $allowed_tags);
    }
    return $output; 
  }
}
