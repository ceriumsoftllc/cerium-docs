<?php

/**
 * @file
 * Views definitions for 'cerium_docs_core_content_types'
 */
 
/**
 * Implementation of hook_views_handlers() to register all of the basic handlers
 * views uses.
 */
function cerium_docs_update_policy_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'cerium_docs_update_policy'),
    ),
    'handlers' => array(
      // The name of my handler
      'cerium_docs_update_policy_handler_update_status_field' => array(
        // The name of the handler we are extending.
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
 
/**
 * Implements hook_views_data().
 */
function cerium_docs_update_policy_views_data() {
  $data = array();
  // Add custom field
  $data['node']['cerium_docs_update_policy_handler_update_status_field'] = array(
    'title' => t('Update Status for Items having attached update policies'),
    'help' => t('Provides a customizable status message if a documentation item needs an update.'),
    'field' => array(
      'handler' => 'cerium_docs_update_policy_handler_update_status_field',
    ),
  );
  return $data;
}
